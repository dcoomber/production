/*
Issue: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/14386

To apply, run this SQL script using gitlab-psql.

This change:
* Fixes the security-definer function "postgres_gin_pending_list_size()" to handle partitioned indexes.
* Renames one of its output fields for clarity.
*/

BEGIN
;

/*
Because this is a set-returning function with an associated pg_type for its result rows, we must
explicitly DROP FUNCTION to implicitly remove its old type, rather than just CREATE OR REPLACE FUNCTION.
*/

DROP FUNCTION public.postgres_gin_pending_list_size()
;

/*
This updated version of the function includes the following changes:
  * Filter to relkind = 'i', to handle partitioned indexes.
    The partitions are still included, but the parent is skipped, because it has no metapage to inspect.
  * Rename output field "pending_pages" to "pending_list_bytes", to correctly indicate the units as bytes, not pages.
*/

CREATE OR REPLACE FUNCTION public.postgres_gin_pending_list_size()
  RETURNS TABLE(index_name text, pending_list_bytes bigint)
  LANGUAGE sql
  SECURITY DEFINER
AS $function$
  WITH gin_indexes AS (
    SELECT ix.indexrelid
    FROM pg_index ix
      LEFT JOIN pg_class i ON ix.indexrelid = i.oid
      LEFT JOIN pg_am a ON i.relam = a.oid
    WHERE a.amname = 'gin' AND i.relkind = 'i'
  )
  SELECT indexrelid::regclass::text AS index_name, n_pending_pages * current_setting('block_size')::bigint AS pending_list_bytes
  FROM gin_indexes, gin_metapage_info(get_raw_page(indexrelid::regclass::text, 0))
$function$
;

/*
Function "postgres_gin_pending_list_size()" is declared as a security-definer function with its owner set to
a postgres superuser because it wraps "get_raw_page()", which can only be called by a superuser.
*/

ALTER FUNCTION public.postgres_gin_pending_list_size() OWNER TO "gitlab-superuser"
;

COMMIT
;
