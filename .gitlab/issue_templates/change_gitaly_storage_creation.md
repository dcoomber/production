# Production Change - Criticality 3 ~C3

### Change Summary

This Production change issue has the steps to build out new storages for Gitaly.

### Change Details

1. **Services Impacted**  - ~"Service::gitaly"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - 120 minutes
1. **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}' -->none

## Detailed steps for the change

### Change Steps - steps to take to execute the change

**Note**: These steps do *not* apply to Praefect systems.

#### Prepare

1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`

#### Create Projects and Provision Gitaly VMs

1. [ ] Create an MR against env-projects to increase the project count, the count will create a new project that will create default 9 new Gitaly VMs ([example MR](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/6964)).
2. [ ] Increase SSD and T2D [quotas](https://console.cloud.google.com/iam-admin/quotas) in the newly created project(s)
    - Update `Persistent Disk SSD (GB)` in `region: us-east1` and `region: us-central1` to `250TB`
    - Update `T2D CPUs` in `region: us-east1` and `region: us-central1` to `640`
2. [ ] Create a new pipeline in config-mgmt for the same environment that will build out the new VMs. This is needed to allow the to-be-created VMs to interact with Vault.
    - Staging: [Run pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/pipelines/new?ref=master&var%5BENV%5D=vault-staging)
    - Production: [Run pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/pipelines/new?ref=master&var%5BENV%5D=vault-production)
2. [ ] Create a new pipeline in config-mgmt for the same environment that will build out the new VMs. This is will create the VMs.
    - Staging: [Run pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/pipelines/new?ref=master&var%5BENV%5D=gstg)
    - Production: [Run pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/pipelines/new?ref=master&var%5BENV%5D=gprd)
3. [ ] Wait for the instances to be built and for Chef to run
    - Staging: [Confirm that chef runs have completed](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gstg%22%2C%20type%3D%22gitaly%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) for new storages
    - Production: [Confirm that chef runs have completed](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gprd%22%2C%20type%3D%22gitaly%22%2C%20fqdn%3D~%22gitaly-.*(PROJECT_ID1%7CPROJECT_ID2).*%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) for new storages

#### Add the new Gitaly projects to Deployer

1. [ ] Create an MR against the deploy-tooling and deployer project that will add the new Project IDs to the list of projects that are used for inventory generation, confirm that the new Gitaly servers are present in the dry-run output of deployer ([deploy-tooling example MR](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/merge_requests/468), [deployer example MR](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/merge_requests/567))

#### Add the new Storages to the Rails and Gitaly configuration

1. [ ] Deactivate gitaly-shard-weights-assigner.
    - Staging: Not needed.
    - Production: Edit the [Production gitaly-shard-weights-assigner job](https://ops.gitlab.net/gitlab-com/gl-infra/gitaly-shard-weights-assigner/-/pipeline_schedules) and mark it as inactive.
1. [ ] Create an MR against chef-repo that adds the new storages to the environment Chef managed configuration. Note it will take around 30 minutes for Chef to converge ([example MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4109)).
2. [ ] Create an MR against k8s-workloads/gitlab-com that will add the new storage to the K8s configuration ([example MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/3190)).

#### Verify that new projects can be created on the new storages

1. [ ] Run the following `glsh` command to confirm that the new storages are present
    - Staging: `glsh gitaly storages list -e gstg`
    - Production: `glsh gitaly storages list -e gprd`
2. [ ] Run the following `glsh` command to confirm that new projects can be created on all of the new storages
    - Staging: `glsh gitaly storages validate -e gstg gitaly-0{1,2,3,4,5,6,7,8,9}-stor-gstg.c.<PROJECT>.internal`
    - Production: `glsh gitaly storages validate -e gprd gitaly-0{1,2,3,4,5,6,7,8,9}-stor-gprd.c.<PROJECT>.internal`

#### Update the project weights so that the storages take new projects

1. [ ] Assign weights to the new storages
    - Staging: With an admin account, navigate to [Repository Storage Settings](https://staging.gitlab.com/admin/application_settings/repository) and set the weight to `100` for the new storages.
    - Production: Activate the [Production gitaly-shard-weights-assigner job](https://ops.gitlab.net/gitlab-com/gl-infra/gitaly-shard-weights-assigner/-/pipeline_schedules) then click the Play button.
2. [ ] Confirm the new weights are set by the weight assigner
    - Staging: `glsh gitaly storages list -e gstg`
    - Production: `glsh gitaly storages list -e gprd`
3. [ ] Check the links in the Monitoring section below to validate RPS, Errors and node metrics on the Gitaly Dashboard.

#### Wrapping up

1. [ ] Set label ~"change::complete" `/label ~change::complete`

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*It is estimated that this will take 5m to complete*

1. [ ] Rollback this change by assigning a zero weight to the new storages
    - Staging: With an admin account, navigate to [Repository Storage Settings](https://staging.gitlab.com/admin/application_settings/repository) and set the weight to `0` for the new storages.
    - Production: With an admin account, navigate to [Repository Storage Settings](https://gitlab.com/admin/application_settings/repository) and set the weight to `0` for the new storages.
2. [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

- Completed Chef runs: [Staging](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gstg%22%2C%20type%3D%22gitaly%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) / [Production](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gprd%22%2C%20type%3D%22gitaly%22%2C%20fqdn%3D~%22gitaly-.*(PROJECT_ID1%7CPROJECT_ID2).*%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
- Gitaly Dashboard: [Staging](https://dashboards.gitlab.net/d/gitaly-main/gitaly-overview?from=now-6h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&orgId=1) / [Production](https://dashboards.gitlab.net/d/gitaly-main/gitaly-overview?from=now-6h%2Fm&to=now%2Fm&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&orgId=1)
- Gitaly RPS by FQDN: [Staging](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(fqdn)%20(%0A%20%20rate(gitaly_service_client_requests_total%7Benv%3D%22gstg%22%2Cenvironment%3D%22gstg%22%2Cfqdn!%3D%22%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%0A)%0A%20%3E%200&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) / [Production](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(fqdn)%20(%0A%20%20rate(gitaly_service_client_requests_total%7Benv%3D%22gprd%22%2Cenvironment%3D%22gprd%22%2Cfqdn%3D~%22gitaly-.*(PROJECT_ID1%7CPROJECT_ID2).*%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%0A)%0A%20%3E%200&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
- Gitaly Errors by FQDN: [Staging](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(fqdn)%20(%0A%20%20label_replace(rate(gitaly_service_client_requests_total%7Benv%3D%22gstg%22%2Cenvironment%3D%22gstg%22%2Cfqdn!%3D%22%22%2Cgrpc_code!~%22AlreadyExists%7CCanceled%7CDeadlineExceeded%7CFailedPrecondition%7CInvalidArgument%7CNotFound%7COK%7CPermissionDenied%7CResourceExhausted%7CUnauthenticated%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%2C%20%22_c%22%2C%20%220%22%2C%20%22%22%2C%20%22%22)%0A%20%20or%0A%20%20label_replace(rate(gitaly_service_client_requests_total%7Bdeadline_type!%3D%22limited%22%2Cenv%3D%22gstg%22%2Cenvironment%3D%22gstg%22%2Cfqdn!%3D%22%22%2Cgrpc_code%3D%22DeadlineExceeded%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%2C%20%22_c%22%2C%20%221%22%2C%20%22%22%2C%20%22%22)%0A)%0A%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) / [Production](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(fqdn)%20(%0A%20%20label_replace(rate(gitaly_service_client_requests_total%7Benv%3D%22gprd%22%2Cenvironment%3D%22gprd%22%2Cfqdn%3D~%22gitaly-.*(PROJECT_ID1%7CPROJECT_ID2).*%22%2Cgrpc_code!~%22AlreadyExists%7CCanceled%7CDeadlineExceeded%7CFailedPrecondition%7CInvalidArgument%7CNotFound%7COK%7CPermissionDenied%7CResourceExhausted%7CUnauthenticated%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%2C%20%22_c%22%2C%20%220%22%2C%20%22%22%2C%20%22%22)%0A%20%20or%0A%20%20label_replace(rate(gitaly_service_client_requests_total%7Bdeadline_type!%3D%22limited%22%2Cenv%3D%22gprd%22%2Cenvironment%3D%22gprd%22%2Cfqdn%3D~%22gitaly-.*(PROJECT_ID1%7CPROJECT_ID2).*%22%2Cgrpc_code%3D%22DeadlineExceeded%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%2C%20%22_c%22%2C%20%221%22%2C%20%22%22%2C%20%22%22)%0A)%0A%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

/label ~"C3"
/label ~"change::unscheduled"
/label ~"change"

<!--
   The following labels are temporary while we provision new storages
   for the DisasterRecovery effort to migrate Gitaly to new projects
-->
/label ~"WG::DisasterRecovery"
/confidential
/epic https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1054
