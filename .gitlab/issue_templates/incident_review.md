<!--

The purpose of this Incident Review is to serve as a classroom to help us better understand the root causes of an incident. Treating it as a classroom allows us to create the space to let us focus on devising the mechanisms needed to prevent a similar incident from recurring in the future. A root cause can **never be a person** and this Incident Review should be written to refer to the system and the context rather than the specific actors. As placeholders for names, consider the usage of nouns like "technician", "engineer on-call", "developer", etc..

-->
## Key Information

<!--- 
Customers Affected: If numbers are available, use them. Link the number to a metric if possible. If numbers are not easy to generate, a text description is fine - "All users with repositories on file-01 node"
Requests Affected: If numbers are available, use them. Link the number to a metric if possible. If numbers are not easy to generate, a text description is fine - "Any requests from Australia during the CloudFlare outage"
Incident Severity: Copy from the incident. Use the severity at the end of the incident, not the beginning. Use the label rather than text - "~Severity::*"
Start Time: Use the time when service started to be effected. Not the time the incident was opened (if they are different)
End Time: Use the time when the incident was mitigated. Don't include the time in a monitoring state unless the monitoring indicated continuing problems.
Total Duration: Use the [Platform Metrics Dashboard](https://dashboards.gitlab.net/d/general-triage/general-platform-triage?orgId=1) to look at appdex and SLO violations.
Link to Incident Issue: Link to Incident Issue
--->

| Metric | Value  |
| ------ | ------ |
| Customers Affected | |
| Requests Affected | |
| Incident Severity | |
| Start Time | |
| End Time | |
| Total Duration | |
| Link to Incident Issue | |

## Summary

<!-- The summary should be a few sentences summarizing the incident. This should be the TL;DR section -->

## Details

<!-- The details section should be a few paragraphs of narrative which tells the story of the incident, including the sequence of events, actions taken, and any relevant observations or findings. It's not necessary to duplicate the timeline which should be in the incident issue -->

## Outcomes/Corrective Actions

<!-- This section should be used to link any corrective action issues or epics which have been created as a result of this incident. If there are vague learnings which do not have an obvious action, use the "What can be improved" section below -->

1. ...

## Learning Opportunities

### What went well?

<!--
Use this section to highlight what went well during the incident. Capturing this helps understand informal
processes and expertise, and enables undocumented knowledge to be shared.

_example:_
1. We quickly discovered a recently changed feature flag through the event log which enabled fast mitigation of the impact, as well as pulling in the engineer involved to further diagnose.
2. We escalated through dev escalations, which brought in Person X. They knew that Person Y had expertise with the component in question, which enabled faster diagnosis.
-->

1. ...

### What was difficult?

<!--
Use this section to highlight opportunities for improvement discovered during the incident. Capturing this helps understand informal
processes and expertise, and enables undocumented knowledge to be shared. If the improvement seems like a simplet change, consider adding it as a corrective action above instead. This section can be empty if all of the improvements can have corrective actions.

_example:_
1. The runbooks for this service are out of date and did not contain the information necessary to troubleshoot the incident.
2. The incidnet happened at a time when nobody with expertise on the service was available.
-->

1. ..

## Review Guidelines

This review should be completed by the team which owns the service causing the alert. That team has the most context around what caused the problem and what information will be needed for an effective fix. The EOC or IMOC may create this issue, but unless they are also on the service owning team, they should assign someone from that team as the DRI.

### For the person opening the Incident Review

- [ ] Set the title to `Incident Review: (Incident issue name)`
- [ ] Assign a `Service::*` label (most likely matching the one on the incident issue)
- [ ] Set a `Severity::*` label which matches the incident
- [ ] In the `Key Information` section, make sure to include a link to the incident issue
- [ ] Find and Assign a DRI from the team which owns the service (check their slack channel or assign the team's manager) **The DRI for the incident review is the issue assignee.**
- [ ] Announce the incident review in the incident channel on Slack.

```
:mega: @here An incident review issue was created for this incident with <USER> assigned as the DRI.
If you have any review feedback please add it to <ISSUE_LINK>.
```

### For the assigned DRI

- [ ] Fill in the remaining fields in the `Key Information` section, using the incident issue as a reference. Feel free to ask the EOC or other folks involved if anything is difficult to find.
- [ ] If there are metrics showing `Customers Affected` or `Requests Affected`, link those metrics in those fields
- [ ] Create a few short sentences in the Summary section summarizing what happened (TL;DR)
- [ ] Use the description section to write a few paragraphs explaining what happened
- [ ] Link any corrective actions and describe any other actions or outcomes from the incident
- [ ] Consider the implications for self-managed and Dedicated instances. For example, do any bug fixes need to be backported?
- [ ] Add any appropriate labels based on the incident issue and discussions
- [ ] Once discussion wraps up in the comments, summarize any takeaways in the details section
- [ ] Close the review before the due date

/label ~"incident-review"
