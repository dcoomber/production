# Production Change - Criticality 2 ~C2

## Change Summary

This production issue is to be used for Gamedays as well as recovery in case of a zonal outage.
It outlines the steps to be followed when adding new patroni replicas in a new zone.

### Gameday execution roles and details

| Role | Assignee |
|---|---|
| Change Technician | <!-- woodhouse: '`@{{ .Username }}`' --> |
| Change Reviewer | <!-- woodhouse: '@{{ .Reviewer }}' --> |

- **Services Impacted**  - ~"Service::PatroniV16" ~"Service::PatroniCiV16" ~"Service::PatroniRegistryV16"
- **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' --> 90 minutes

{+ Provide a brief summary indicating the affected zone +}

## [**For Gamedays only**] Preparation Tasks

1. [ ] One week before the gameday make an announcement on slack [production_engineering](https://gitlab.enterprise.slack.com/archives/C03QC5KNW5N) channel. Consider also sharing this post in the appropriate environment channels, [staging](https://gitlab.enterprise.slack.com/archives/CLM200USV) or [production](https://gitlab.enterprise.slack.com/archives/C101F3796).

    - **Example message:**

    ```code
      Next week on [DATE & TIME] we will be executing a Patroni and PGBouncer zonal outage game day. The process will involve provisioning additional capacity for these services in alternate zones. This should take approximately 90 minutes, the aim for this exercise is to test our disaster recovery capabilities and measure if we are still within our RTO & RPO targets set by the [DR working group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/) for GitLab.com.
      See LINK_TO_CHANGE_ISSUE for details.
    ```

1. [ ] Add an event to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
1. [ ] Notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.
1. [ ] Notify the eoc on _Slack_ by mentioning `@sre-oncall` and referencing this issue and wait for approval by adding the ~eoc_approved label.
    - **Example message:**

    ```code
    @release-managers or @sre-oncall LINK_TO_THIS_CR is scheduled for execution. We will be adding Patroni and PGBouncer replicas for approximately 30 minutes. Kindly review and approve the CR
    ```

1. [ ] Post an FYI link of the slack message to the [#test-platform](https://gitlab.enterprise.slack.com/archives/C3JJET4Q6) channel on slack.

## Detailed steps for the change

### Change Steps - steps to take to execute the change

#### Execution

1. [ ] If you are conducting a practice (Gameday) run of this, consider starting a recording of the process now.
1. [ ] Note the start time in UTC in a comment to record this process duration.
1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
1. [ ] Create the MR to provision Patroni and PGBouncer replicas.
    - [ ] Create the MR
      - **Example MRs:**
        - `gstg` <https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/9086>
        - `gprd` TBD
      - **Note:**
        - `atlantis plan/apply` might fail if there are missing snapshots , a quick look at the runbook [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/alerts/GCPScheduledSnapshots.md?ref_type=heads) might be helpful
    - [ ] Get the MR approved
    - [ ] Apply the MR, Note the time in UTC in a comment when you run `atlantis apply` , we would need this to calculate the `VM Provision time`

    ❗❗ **NOTE:** ❗❗ _Merging the changes into master can take a while to complete, it can take up to 30 minutes before you are able to log in to the new replicas._

#### Validation of the Patroni replicas

1. [ ] Wait for the instances to be built and Chef to converge.
    - Staging: [Confirm that chef runs have completed](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22m2b%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_last_run_timestamp_seconds%7Benv%3D%5C%22gstg%5C%22,%20type%3D~%5C%22patroni%7Cpatroni-ci%7Cpatroni-registry%5C%22,%20fqdn%21~%5C%22patroni-main-v16-10.%2A-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1) for new patroni replicas it can take up to 30 minutes before they show up. A sudden increase in the y-axis of the time across the new replicas may signify that Chef has run successfully.
      <details><summary>Trouble shooting tips</summary>

      - Tail the serial output to confirm that the start up script executed successfully for all the new Patroni and PGBouncer nodes and wait until there is no [replication lag](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22jk0%22%3A%7B%22datasource%22%3A%22mimir-gitlab-gstg%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22max%28postgres%3Apg_replication_lag_bytes%7Benv%3D%5C%22gstg%5C%22%7D%29+by+%28fqdn%29%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22mimir-gitlab-gstg%22%7D%2C%22editorMode%22%3A%22code%22%2C%22legendFormat%22%3A%22__auto%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-30m%22%2C%22to%22%3A%22now%22%7D%7D%7D&orgId=1).
      `gcloud compute --project=$project instances tail-serial-port-output $instance_name --zone=$zone --port=1`
      the variables `$project` represents the project `gitlab-staging-1` for the patroni replicas, `$instance_name` represent the instance e.g `patroni-main-v16-106-db-gstg` and `$zone` represents the recovery zone e.g `us-east1-b`.
      - We could also tail bootstrap logs example: `tail -f /var/tmp/bootstrap-20231108-133642.log`, if we see a log that states `bootstrap completed` we can move on to the next steps.

    </details>

1. [ ] Once the patroni replicas are ready ssh into each replica and validate that Patroni is running:
    - `sudo gitlab-patronictl list`
    - Restart Patroni if not:
      - `sudo systemctl enable patroni && sudo systemctl start patroni`
1. [ ] Review graphs for each of the database clusters to ensure traffic is being distributed to the new instances.
    1. [ ] [Main](https://dashboards.gitlab.net/d/patroni-main/patroni3a-overview?orgId=1&var-PROMETHEUS_DS=e58c2f51-20f8-4f4b-ad48-2968782ca7d6&var-environment=gstg&viewPanel=74)
    2. [ ] [CI](https://dashboards.gitlab.net/d/patroni-ci-main/patroni-ci3a-overview?orgId=1&var-PROMETHEUS_DS=e58c2f51-20f8-4f4b-ad48-2968782ca7d6&var-environment=gstg&viewPanel=74)
    3. [ ] [Registry](https://dashboards.gitlab.net/d/patroni-registry-main/patroni-registry3a-overview?orgId=1&var-PROMETHEUS_DS=e58c2f51-20f8-4f4b-ad48-2968782ca7d6&var-environment=gstg&viewPanel=36)
1. [ ] Validate error rates are nominal across the rails services.
    1. [ ] [Web](https://dashboards.gitlab.net/d/web-main/web3a-overview?orgId=1&var-PROMETHEUS_DS=e58c2f51-20f8-4f4b-ad48-2968782ca7d6&var-environment=gstg&var-stage=main)
    2. [ ] [API](https://dashboards.gitlab.net/d/api-main/api3a-overview?orgId=1&var-PROMETHEUS_DS=e58c2f51-20f8-4f4b-ad48-2968782ca7d6&var-environment=gstg&var-stage=main)

#### Switchover Patroni Leader

1. [ ] Identify and note the current Leader for `main` Patroni shard (we can omit `ci`, `registry` and `embedding`, as the process is the same and will not give us any additional benefits under this gameday)

- [ ] Run `sudo gitlab-patronictl list` command on any patroni-main node. This will provide the details about the cluster and the current leader

1. [ ] Run `sudo gitlab-patronictl switchover` command on any patroni-main node

- [ ] Run `sudo gitlab-patronictl list` command on any patroni-main node again and observe if the Leader changed

#### Validation of the Leader switchover

- [PostgreSQL Replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global&from=now-3h&to=now)
- [Monitor what pgbouncer pool has connections](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pc6%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28pgbouncer_pools_client_active_connections%7Benv%3D%5C%22gstg%5C%22,%20user%3D%5C%22gitlab%5C%22,%20type%3D~%5C%22patroni%7Cpatroni-main-v16%5C%22%7D%29%20by%20%28fqdn,database%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [Review WRITES going to the cluster](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%28increase%28pg_stat_user_tables_n_tup_ins%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,type%3D~%5C%22patroni%7Cpatroni-main-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_del%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,type%3D~%5C%22patroni%7Cpatroni-main-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_upd%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,type%3D~%5C%22patroni%7Cpatroni-main-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cnand%20on%28instance%29%20pg_replication_is_replica%3D%3D0%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [Review READs going to the cluster](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%28sum%28rate%28pg_stat_user_tables_seq_tup_read%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,type%3D~%5C%22patroni%7Cpatroni-main-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28fqdn%29%29%20and%20on%28fqdn%29%20pg_replication_is_replica%3D%3D1%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

1. [ ] Note the end time in UTC in a comment to record the completion of this change

#### [**For Gamedays only**] Clean up

**Patroni Leader switchover**:

- [ ] Identify and note the current Leader for `main` Patroni shard
  - [ ] Run `sudo gitlab-patronictl list` command on any patroni-main node
- [ ] Run `sudo gitlab-patronictl switchover` command on any patroni-main node and specify the pre-change leader as a target.
  - [ ] Run `sudo gitlab-patronictl list` command on any patroni-main node again and observe if the Leader changed

- [ ] Set all new Patroni nodes to maintenance mode. Adjust the replacement nodes names in the below scripts before the execution
  - Set maintenance mode roles:

    ```bash
    for node in patroni-ci-v16-{105,106} patroni-embedding-04 patroni-main-v16-{105,106} patroni-registry-v16-05; do knife node run_list add "${node}-db-gstg.c.gitlab-staging-1.internal" 'role[gstg-base-db-patroni-maintenance]' -y; done
    ```

  - Run chef-client:

    ```bash
    for node in patroni-ci-v16-{105,106} patroni-embedding-04 patroni-main-v16-{105,106} patroni-registry-v16-05; do echo "${node}-db-gstg.c.gitlab-staging-1.internal" ; done | xargs -P0 -I '{}' ssh {} 'sudo chef-client'
    ```

    ❗❗**NOTE**❗❗ The hostnames in these commands are examples. Be sure to update them to match the newly created nodes accordingly.

- [ ] Open and merge a MR to remove the nodes added for this game day.

    ❗❗**NOTE**❗❗ _when removing the nodes created as part of this gameday, you may need to add the `~approve_policies` label and comment `atlantis approve_policies` in the MR to bypass the policies before applying with `atlantis apply`_.

#### Wrapping up

- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is complete.
- [ ] Set label ~"change::complete" `/label ~change::complete`
- [ ] Compile the real time measurement for all the new Patroni and PGBouncer nodes by running the [script](https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/find-bootstrap-duration.sh?ref_type=heads), for example:

```bash
# For new Patroni Nodes
for node in patroni-ci-v16-{105,106} patroni-embedding-04 patroni-main-v16-{105,106} patroni-registry-v16-05; do ./scripts/find-bootstrap-duration.sh $node-db-gstg.c.gitlab-staging-1.internal ; done
```

```bash
#For PGBouncer Nodes
for node in pgbouncer-{07,08}-db-gstg pgbouncer-ci-04-db-gstg pgbouncer-sidekiq-04-db-gstg pgbouncer-sidekiq-ci-04-db-gstg; do ./scripts/find-bootstrap-duration.sh $node-db-gstg.c.gitlab-staging-1.internal ; done
```

Update the [Recovery Measurements Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/recovery-measurements.md?ref_type=heads).

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

#### _It is estimated that this will take 5m to complete_

- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is aborted.
- [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

- Completed Chef runs: [Staging](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22m2b%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_last_run_timestamp_seconds%7Benv%3D%5C%22gstg%5C%22,%20type%3D~%5C%22patroni%7Cpatroni-ci%7Cpatroni-registry%5C%22,%20fqdn%21~%5C%22patroni-main-v16-10.%2A-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- Patroni service: [Staging](https://dashboards.gitlab.net/d/patroni-main/patroni3a-overview?orgId=1&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg)
- PostgreSQL Overview: [Staging](https://dashboards.gitlab.net/d/000000144/postgresql-overview?orgId=1&var-prometheus=mimir-gitlab-gstg&var-environment=gstg&var-type=patroni)
- Replication Lag: [Staging](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22i67%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22pg_replication_lag%7Benv%3D%5C%22gstg%5C%22,fqdn%3D~%5C%22patroni-main-v16-.%2A%5C%22%7D%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:

- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:

- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
    - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed upon with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, use the `@sre-oncall` handle in slack
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results are noted in a comment on this issue.
  - A dry-run has been conducted and results are noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed before the change is rolled out. (In the #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In the #production channel, mention `@release-managers` and this issue and await their acknowledgement.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.

/label ~"C2"
/label ~"change::unscheduled"
/label ~"change"
/label ~"gamedays"
