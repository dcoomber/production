# Production Change - Criticality 2 ~C2

## Change Summary

This production issue is to be used for Gamedays as well as recovery in case of a zonal outage.
It outlines the steps to be followed when testing traffic shifts due to zonal outages.
Hopefully corrective actions from testing will help us build new steps to take during a real outage.

### Gameday execution roles and details

| Role | Assignee |
|---|---|
| Change Technician | <!-- woodhouse: '`@{{ .Username }}`' --> |
| Change Reviewer | <!-- woodhouse: '@{{ .Reviewer }}' --> |

- **Services Impacted**  - TBD
- **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' --> 90 minutes
- **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}'--> 30 minutes

{+ Provide a brief summary indicating the affected zone +}
<!-- e.g Restricting traffic to operating in two remaining zones due to a zonal outage in us-east1-d -->

## [**For Gamedays only**] Preparation Tasks

1. [ ] One week before the gameday make an announcement on slack [production_engineering](https://gitlab.enterprise.slack.com/archives/C03QC5KNW5N) channel. Consider also posting this in the appropriate environment channels, [staging](https://gitlab.enterprise.slack.com/archives/CLM200USV) or [production](https://gitlab.enterprise.slack.com/archives/C101F3796).

    - **Example message:**

    ```code
    Next week on [DATE & TIME] we will be executing a Traffic Routing game day. The process will involve moving traffic away from a single zone in `gstg` to test our disaster recovery capabilities and measure if we are still within our RTO & RPO targets set by the  [DR working group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/) for GitLab.com.
    See <https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17274>
    ```

1. [ ] One week before the gameday notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.
1. [ ] Add an event to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
1. [ ] Post an FYI link of the slack message to the [#test-platform](https://gitlab.enterprise.slack.com/archives/C3JJET4Q6) channel on slack.
1. [ ] Before commencing the change notify the eoc on _Slack_ by mentioning `@sre-oncall` and referencing this issue and wait for approval by adding the ~eoc_approved label.
1. [ ] Before commencing the change notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.

    - **Example message:**

    ```code
    @release-managers or @sre-oncall LINK_TO_THIS_CR is scheduled for execution.
    We will be diverting traffic away from a single zone in gstg to test our disaster recovery capabilities and measure if we are still within our RTO & RPO targets. Kindly review and approve the CR
    ```

## Detailed steps for the change

### Change Steps - steps to take to execute the change

#### Execution

1. [ ] If you are conducting a practice (Gameday) run of this, consider starting a recording of the process now.

1. [ ] Note the start time in UTC in a comment to record this process duration.

1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`

1. Reconfigure the regional cluster to exclude the affected zone by setting `regional_cluster_zones` in Terraform to a list of zones that are not impacted
    - [ ] Create the MR to update the `regional_cluster_zones` , while _emulating_ a zonal outage make sure to create replacement nodes.
      ```bash
      #Command to check HAProxy nodes in a particular zone

      knife search node 'name:haproxy* AND chef_environment:gstg AND zone:*us-east1-d'
      ```
    - **Example MR** <https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/8963>.
    - [ ] Get the MR approved
    - [ ] Merge the MR
    - [ ] Create the MR for chef update for the new replaced nodes:
    - [**Example MR**](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4936).
    - [ ] Get the MR approved
    - [ ] Make sure the MR to update the `regional_cluster_zones` has been merged and the new nodes provisioned before merging the chef MR
      - [ ] Trigger a chef-client run on all HAProxy nodes once the chef-repo pipeline has completed:

        ```
        knife ssh 'name:haproxy* AND chef_environment:gstg' 'sudo chef-client'
        ```

    ❗❗**NOTE**❗❗ _This takes a while to complete (approximately 30 minutes)

1. [ ] Remove the HAProxy instances from the GCP load balancers (this must be done AFTER the above terraform change is applied):
    - Check out the chef-repo repository at: <git@gitlab.com>:gitlab-com/gl-infra/chef-repo.git

    ```bash
    cd chef-repo
    ./bin/manage-gcp-lb-haproxy
    ```

      - The script will prompt for an environment, then a zone. Select the values correlating to the zone we are removing traffic from.

1. [ ] Disable the HAproxy servers:

    ```bash
    cd chef-repo
    ./bin/disable-server gstg us-east1-d
    ```

    - [ ] Validate that all servers in the affected zone have their state set to `MAINT`. You can use [this query](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22yju%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%28haproxy_server_status%7Benv%3D%5C%22gstg%5C%22,%20server%3D~%5C%22.%2Aus-east1-d.%2A%5C%22%7D%29%20by%20%28state%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-30m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1) to confirm that there are zero backends in the `UP` state for the zone that you are evacuating.

1. [ ] Note the conclusion time in UTC in a comment to record this process duration.

#### Validation

Once traffic is restricted to our remaining two zones, let's identify the impact and look for problems.

- [ ] Do we see a drop in CPU usage in one zone cluster? [GSTG Per Cluster CPU Usage](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%20by%20%28cluster%29%20%28rate%28container_cpu_usage_seconds_total:labeled%7Benv%3D%5C%22gstg%5C%22,namespace%3D%5C%22gitlab%5C%22%7D%5B5m%5D%29%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [ ] Do we see a drop in HPA targets in one zone cluster? [GSTG Per Cluster HPA Target](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%20by%20%28cluster%29%20%28kube_horizontalpodautoscaler_status_current_replicas%7Benv%3D%5C%22gstg%5C%22,namespace%3D%5C%22gitlab%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [ ] [Examine GSTG Rails logs for errors](https://nonprod-log.gitlab.net/app/r/s/005mL)
- [ ] [Examine frontend dashboard for GSTG](https://dashboards.gitlab.net/d/frontend-main/frontend3a-overview?orgId=1&var-PROMETHEUS_DS=mimir-gitlab-gstg&var-environment=gstg&var-stage=main&from=now-1h&to=now)
- [ ] [Examine the connected peers changed with the new introduced nodes](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22yju%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%20%28haproxy_process_connected_peers%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-30m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

#### Wrapping up and cleanup

- [ ] Re-enable the zonal GKE backend cluster in HAProxy

  ```bash
  cd chef-repo
  ./bin/enable-server gstg us-east1-d
  ```

  - [ ] Validate that all servers in the affected zone have their state set to `UP`. You can use [this query](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22yju%22:%7B%22datasource%22:%22mimir-gitlab-gstg%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%28haproxy_server_status%7Benv%3D%5C%22gstg%5C%22,%20server%3D~%5C%22.%2Aus-east1-d.%2A%5C%22%7D%29%20by%20%28state%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-gstg%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-30m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1) to confirm that there are zero backends in the `MAINT` state in the zone that you are returning to service.

- [ ] Open a MR to revert the change on the chef-repo ([Example MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4959/diffs))
  - [ ] Get the MR approved
  - [ ] Merge the MR
  - [ ] Trigger a chef-client run on all HAProxy nodes once the chef-repo pipeline has completed:
        ```
        knife ssh 'name:haproxy* AND chef_environment:gstg' 'sudo chef-client'
        ```
- [ ] Open a MR to revert the change to disable the zone in the regional cluster. (This will also revert the changes made to the GCP loadbalancers with `gcloud` commands)
  - [ ] Get the MR approved
  - [ ] Make sure the MR in chef-repo to remove the Gameday HAProxy nodes from peering has been merged and chef client run on the nodes
  - [ ] Merge the MR
- [ ] Set label ~"change::complete" `/label ~change::complete`
- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is complete.
- [ ] Compile the real time measurement of this process and update the [Recovery Measrements Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/recovery-measurements.md?ref_type=heads).

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

#### _It is estimated that this will take 5m to complete_

- [ ] [Re-enable HAProxy with Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/pipelines/new?ref=main&var%5BENV%5D=gstg)
- [ ] Re-enable HAProxy

  ```bash
  cd chef-repo
  ./bin/enable-server gstg us-east1-d
  ```

- [ ] Set label ~"change::complete" `/label ~change::aborted`
- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise has been aborted.

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:

- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:

- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
    - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed upon with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, use the `@sre-oncall` handle in slack
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results are noted in a comment on this issue.
  - A dry-run has been conducted and results are noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed before the change is rolled out. (In the #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In the #production channel, mention `@release-managers` and this issue and await their acknowledgement.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.

/label ~"C2"
/label ~"change::unscheduled"
/label ~"change"
/label ~"release-blocker"
/label ~"blocks deployments"
/label ~"Deploys-blocked-gstg"
/label ~"gamedays"
